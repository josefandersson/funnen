const jwt = require('jsonwebtoken');
const config = require('../config');

const db = require('../src/database');

const router = require('express').Router();

// GET /user/
router.get('/', (req, res) => {
    if (req.user) {
        res.json(req.user.safe());
    } else {
        res.json({ error: 'not logged in' });
    }
});

// POST /user/name
// Body: name
// Change name
router.post('/name', async (req, res) => {
    if (!req.user) return res.json({ error: 'not logged in' });
    if (!req.body.name == null) return res.json({ error: 'missing name' });

    const newName = req.body.name.length ? req.body.name : null;

    if (req.user.name !== newName) {
        req.user.name = newName;
        await req.user.save();
    }

    res.json({ name: req.user.name });
});

// POST /user/email
// Change email, without change code one is sent to old email
// Body: code?, email?
router.post('/email', async (req, res) => {
    if (req.body.code) {
        // TODO: Get entry from email change table
        // TODO: Check if logged in, if so user must match?
        // TODO: Change the email

        res.json({ error: 'not implemented' });
    } else {
        if (!req.user) return res.json({ error: 'not logged in' });
        if (!req.body.email) return res.json({ error: 'missing email' });
        if (!/^.*?@.*?\..*$/.test(req.body.email))
            return res.json({ error: 'invalid email' });

        // TODO: Send email change email to old email (note ip/country?)
        // TODO: Add entry to email change table

        res.json({ error: 'not implemented' });
    }
});

// POST /user/password
// Change password, without reset code one is sent to old email
// Body: code?, password?, cpassword?
router.post('/password', async (req, res) => {
    if (req.body.code) {
        if (!req.body.password) return res.json({ error: 'missing password' });
        if (!req.body.cpassword)
            return res.json({ error: 'missing cpassword' });
        if (req.body.password !== req.body.cpassword)
            return res.json({ error: 'passwords do not match' });

        // TODO: Get entry from password change table
        // TODO: Check if logged in, if so user must match?
        // TODO: Change the password

        res.json({ error: 'not implemented' });
    } else {
        if (!req.user) return res.json({ error: 'not logged in' });

        // TODO: Send password reset email  (note ip/country?)
        // TODO: Add entry to password change table

        res.json({ error: 'not implemented' });
    }
});

// POST /user/verify
// Verify user account
// Body: code?
router.post('/verify', async (req, res) => {
    if (req.user && req.user.emailVerified)
        return res.json({ error: 'already verified' });

    if (req.body.code) {
        const { userId } = jwt.verify(
            req.body.code,
            config.ACCOUNT_VERIFICATION_SECRET
        );
        if (userId) {
            if (!req.user) {
                const user = await db.User.findOne({ where: { id: userId } });
                if (user) {
                    user.emailVerified = true;
                    user.save();
                    res.json({});
                } else res.json({ error: 'bad user' });
            } else if (req.user.id === userId) {
                req.user.emailVerified = true;
                await req.user.save();
                res.json({});
            } else res.json({ error: 'mismatching user' });
        } else res.json({ error: 'invalid code' });
    }
});

module.exports = router;
