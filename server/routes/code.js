const db = require('../src/database');
const emailer = require('../src/emailer');
const config = require('../config');

const router = require('express').Router();

// POST /code/
// Update visit count for code
// Body: code
router.post('/', async (req, res) => {
    if (!req.body.code) return res.json({ error: 'missing code' });

    try {
        const code = await db.Code.findOne({
            where: { code: req.body.code },
            include: [db.User],
        });

        code.visitCount++;
        await code.save();

        if (code) res.json({ code: { code: code.code } });
        else res.json({ code: null });
    } catch (e) {
        console.warn(e);
        res.json({ error: 'database error' });
    }
});

// POST /code/contact
// Send a message to the owner of a code
// Body: code, message, fromEmail
router.post('/contact', async (req, res) => {
    if (!req.body.code) return res.json({ error: 'missing code' });
    if (!req.body.email) return res.json({ error: 'missing email' });
    if (!req.body.message) return res.json({ error: 'missing message' });

    let code;
    try {
        code = await db.Code.findOne({
            where: { code: req.body.code },
            include: [db.User],
        });
    } catch (e) {
        console.warn(e);
        res.json({ error: 'database error' });
    }

    if (!code) return res.json({});

    if (!code.isLost) return res.json({ error: 'code not lost' });

    if (!req.user || req.user.id !== code.userId) {
        if (!code.user.lostsLeft) return res.json({ error: 'code abandoned' });

        // TODO: Count as a contact
        code.lostCount++;
        await code.save();
        code.user.lostsLeft--;
        await code.user.save();
    }

    await emailer.sendContactMail(
        code,
        req.body.email,
        req.body.message,
        !!req.body.share
    );

    res.json({});
});

// GET /code/all
// Get all codes for current user
router.get('/all', async (req, res) => {
    if (!req.user) return res.json({ error: 'not logged in' });

    try {
        const codes = await req.user.getCodes();
        res.json({
            codes: codes.map(c => c.safe()),
        });
    } catch (e) {
        console.warn(e);
        res.json({ error: 'database error' });
    }
});

// POST /code/new
// Generate new code
router.post('/new', async (req, res) => {
    if (!req.user) return res.json({ error: 'not logged in' });

    try {
        const code = await db.Code.create({
            code: randomCode(),
            userId: req.user.id,
        });
        res.json({ code: code.code });
    } catch (e) {
        if (error.name === 'SequelizeUniqueConstraintError')
            console.warn('Code already exists...');
        console.warn(e);
        res.json({ error: 'database error' });
    }
});

// POST /code/delete
// Delete code
// Body: codeId
router.post('/delete', async (req, res) => {
    if (!req.user) return res.json({ error: 'not logged in' });
    if (req.body.codeId == null) return res.json({ error: 'missing code id' });

    try {
        const code = await db.Code.findOne({
            where: { id: req.body.codeId, userId: req.user.id },
        });
        if (!code) return res.json({ error: 'code does not exist' });

        await code.destroy();

        res.json({});
    } catch (e) {
        console.warn(e);
        res.json({ error: 'database error' });
    }
});

// POST /code/update
// Update code lost state
// Body: codeId, lost
router.post('/update', async (req, res) => {
    if (!req.user) return res.json({ error: 'not logged in' });
    if (req.body.codeId == null) return res.json({ error: 'missing code id' });

    try {
        const code = await db.Code.findOne({
            where: { id: req.body.codeId, userId: req.user.id },
        });
        if (!code) return res.json({ error: 'not owner' });
        
        code.isLost = !!req.body.isLost;
        await code.save();

        res.json({ isLost: code.isLost });
    } catch (e) {
        console.warn(e);
        res.json({ error: 'database error' });
    }
});

/**
 * Generate a code
 * @returns {String} Code
 */
const randomCode = () => {
    const randomChar = () =>
        config.VALID_CHARS[
            Math.floor(Math.random() * config.VALID_CHARS.length)
        ];

    let code = '';
    for (let i = 0; i < config.CODE_LENGTH; i++) code += randomChar();

    return code;
};

module.exports = router;
