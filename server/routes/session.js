const crypto = require('crypto');
const router = require('express').Router();

const config = require('../config');
const db = require('../src/database');

// Validate session
router.use(async (req, res, next) => {
    let sid;
    const ipRes = await db.IpAddress.findOrCreate({
        where: { ip: req.ipInfo.ip },
        defaults: { ip: req.ipInfo.ip, country: req.ipInfo.country },
    });
    const ipAddress = ipRes[0];
    if (ipAddress.banned) {
        res.send('Your ip is banned from using this website.');
        return;
    }
    if (!(req.cookies && req.cookies.sid) || req.cookies.sid.length !== 88) {
        sid = crypto.randomBytes(64).toString('base64');
        req.user = null;
        req.cookies.sid = sid;
        res.cookie('sid', sid, {
            maxAge: config.SESSION_INACTIVE_TIME,
            sameSite: 'strict',
            secure: true,
        });
    } else {
        sid = req.cookies.sid;
        const user = await wasActive(sid);
        if (user) {
            if (user.lastIpId !== ipAddress.id) {
                user.lastIpId = ipAddress.id;
                await user.save();
            }
            if (user.countryLock && user.country !== req.ipInfo.country) {
                await auth.logout(user, sid);
            } else {
                req.user = user;
            }
            res.cookie('sid', sid, {
                maxAge: config.SESSION_INACTIVE_TIME,
                sameSite: 'strict',
                secure: true,
            });
        } else {
            res.cookie('sid', sid, {
                maxAge: 9000000000,
                sameSite: 'strict',
                secure: true,
            });
        }
    }
    next();
});

/**
 * Update time until expiration for session
 * @param {String} sessionId
 * @returns {db.User} User associated with session
 */
async function wasActive(sessionId) {
    try {
        const session = await db.Session.findOne({
            where: { id: sessionId },
            include: db.User,
        });
        session.wasActive();
        return session.user;
    } catch (error) {}
    return null;
}

module.exports = router;
