const { TextEncoder } = require('util');
const { Op } = require('sequelize');

const db = require('../src/database');
const emailer = require('../src/emailer');
const config = require('../config');

const router = require('express').Router();

// POST /login
// body: email, password
router.post('/login', async (req, res) => {
    if (req.user) return res.json({ error: 'already logged in' });

    try {
        const user = await login(
            req.body.email,
            req.body.password,
            req.cookies.sid,
            req.ipInfo.ip
        );
        res.json({ user: user.safe() });
    } catch (error) {
        if (
            error === 'unknown user' ||
            error === 'wrong password' ||
            error === 'bad session id' ||
            error === 'too many login attempts'
        ) {
            res.json({ error });
        } else {
            console.log('could not login:', error);
            res.json({ error: 'database error' });
        }
    }
});

// POST /register
// body: email, password, cpassword
router.post('/register', async (req, res) => {
    if (req.user) return res.json({ error: 'already logged in' });
    if (!req.body.email) return res.json({ error:'missing email'});
    if (req.body.password !== req.body.cpassword)
        return res.json({ error: 'passwords do not match' });

    const lenOffset = checkPassLength(req.body.password);
    if (lenOffset < 0) return res.json({ error: 'password too short' });
    if (lenOffset > 0) return res.json({ error: 'password too long' });

    try {
        const user = await register(
            req.body.email,
            req.body.password,
            req.cookies.sid,
            req.ipInfo.ip,
            req.ipInfo.country
        );
        res.json({ user: user.safe() });
    } catch (error) {
        res.json({ error });
    }
});

// POST /logout
// Logout from current session
router.post('/logout', async (req, res) => {
    try {
        await logout(req.user, req.cookies.sid);
        res.json({ });
    } catch (error) {
        if (error === 'bad session id' || error === 'not logged in')
            res.json({ error });
        else res.json({ error: 'database error' });
    }
});

// POST /logout_all
// Logout from all sessions
router.post('/logout_all', async (req, res) => {
    try {
        await logoutAll(req.user);
        res.json({});
    } catch (error) {
        if (error === 'not logged in') res.json({ error });
        else res.json({ error: 'database error' });
    }
});

/**
 * Check if pass length is ok
 * @param {String} pass Password
 * @returns {Number} 0 if ok, negative if short, positive if long
 */
function checkPassLength(pass) {
    const len = new TextEncoder().encode(pass).length;
    if (len < config.PASS_MIN_BYTES) return len - config.PASS_MIN_BYTES;
    if (len > config.PASS_MAX_BYTES) return len - config.APP.PASS_MAX_BYTES;
    return 0;
}

/**
 * Register new user with credentials
 * @param {String} emailAddress
 * @param {String} password
 * @param {String} sessionId
 * @param {String} ip
 * @param {String} country 2-char country code
 * @returns Registered user object
 */
async function register(emailAddress, password, sessionId, ip, country = '--') {
    if (!sessionId || sessionId.length !== 88) throw 'bad session id';

    try {
        // Save ip
        const ipAdr = await db.IpAddress.findOrCreate({
            where: { ip },
            defaults: { ip, country },
        });
        const ipId = ipAdr[0].dataValues.id || ipAdr[0].id;

        // Create user
        const user = await db.User.create(
            {
                emailAddress,
                password,
                registrationIpId: ipId,
                lastIpId: ipId,
                country,
                sessions: [{ id: sessionId }],
            },
            { include: db.Session }
        );

        emailer.sendRegistrationMail(user);

        return user;
    } catch (error) {
        if (error.name === 'SequelizeUniqueConstraintError')
            throw 'user already exists';
        if (error.name === 'SequelizeValidationError') throw 'bad email'; // TODO: Check email before hand OR notify user of error here
        console.log(error);
        console.log('Could not register:', error);
        throw 'database error';
    }
}

/**
 * Attempt to login to user with credentials
 * @param {String} emailAddress
 * @param {String} password
 * @param {String} sessionId
 * @param {String} ip
 * @returns
 */
async function login(emailAddress, password, sessionId, ip) {
    if (sessionId.length !== 88) throw 'bad session id';

    // Count previous failed login attempts from ip and for email
    const createdAtGt = Date.now() - 3600000;
    let numAttempts = await db.LoginAttempt.sum('count', {
        where: {
            [Op.or]: [{ emailAddress }, { ip }],
            createdAt: { [Op.gt]: createdAtGt },
        },
    });

    // Create this login attempt if doesn't exist
    const res = await db.LoginAttempt.findCreateFind({
        where: { ip, emailAddress, createdAt: { [Op.gt]: createdAtGt } },
        defaults: { ip, emailAddress, createdAt: Date.now() },
    });
    const attempt = res[0];
    const newlyCreated = res[1];
    if (!newlyCreated) attempt.count++;

    // Count this login attempt
    numAttempts++;

    // Prevent too many failed attempts
    if (numAttempts > config.MAX_LOGIN_ATTEMPTS_PER_HOUR) {
        attempt.critical = true;
        await attempt.save();
        throw 'too many login attempts';
    }

    // Check if email exists
    const user = await db.User.findOne({ where: { emailAddress } });
    if (!user) {
        await attempt.save();
        throw 'unknown user';
    }

    // Check if password matches
    if (!user.testPassword(password)) {
        await attempt.save();
        throw 'wrong password';
    }

    // Create user session and save login attempt
    await db.Session.create({ id: sessionId, userId: user.id });
    attempt.userId = user.id;
    await attempt.save();

    return user;
}

/**
 * Logout from session
 * @param {db.User} user
 * @param {String} sessionId
 */
async function logout(user, sessionId) {
    if (!user) throw 'not logged in';
    if (!sessionId || sessionId.length !== 88) throw 'bad session id';

    await db.Session.logout(sessionId);
}

/**
 * Logout from all session associated with user
 * @param {db.User} user
 */
async function logoutAll(user) {
    if (!user) throw 'not logged in';

    await db.Session.destroy({ where: { userId: user.id } });
}

module.exports = router;
