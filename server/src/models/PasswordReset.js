const { Model, DataTypes } = require('sequelize');

class PasswordReset extends Model {}

const init = sequelize => {
    PasswordReset.init(
        {
            code: {
                type: DataTypes.STRING,
                allowNull: false,
            },
        },
        {
            sequelize,
            modelName: 'passwordreset',
        }
    );

    return PasswordReset;
};

module.exports = init;
