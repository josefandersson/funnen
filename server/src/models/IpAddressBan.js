const { Model, DataTypes } = require('sequelize');

class IpAddressBan extends Model {}

const init = sequelize => {
    IpAddressBan.init(
        {
            note: DataTypes.TEXT,
            unbanAt: DataTypes.DATE,

            // ip was unbanned, but have been previously banned
            undone: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
        },
        {
            sequelize,
            modelName: 'ipban',
        }
    );

    return IpAddressBan;
};

module.exports = init;
