const { Model, DataTypes } = require('sequelize');

class Order extends Model {}

const init = sequelize => {
    Order.init(
        {
            amount: {
                type: DataTypes.FLOAT,
                allowNull: false,
            },
            state: {
                type: DataTypes.ENUM('placed'),
                allowNull: false
            }
        },
        {
            sequelize,
            modelName: 'order',
            paranoid: true,
        }
    );

    return Order;
};

module.exports = init;
