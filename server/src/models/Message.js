const { Model, DataTypes } = require('sequelize');

class Message extends Model {}

const init = sequelize => {
    Message.init(
        {
            message: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            // Message is from finder to owner
            toOwner: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
            sentAt: DataTypes.DATE,
        },
        {
            sequelize,
            modelName: 'message',
            paranoid: true,
        }
    );

    return Message;
};

module.exports = init;
