const { Model, DataTypes } = require('sequelize');

class IpAddress extends Model {}

const init = sequelize => {
    IpAddress.init(
        {
            ip: {
                type: DataTypes.STRING(36),
                unique: true,
                validate: { isIP: true },
            },
            country: DataTypes.STRING(2),
            banned: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
        },
        {
            sequelize,
            modelName: 'ip',
        }
    );

    return IpAddress;
};

module.exports = init;
