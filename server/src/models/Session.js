const { Sequelize, Model, DataTypes } = require('sequelize');

const config = require('../../config');

class Session extends Model {
    /**
     * Update time until expiration
     */
    async wasActive() {
        this.expires = Date.now() + config.SESSION_INACTIVE_TIME;
        await this.save();
    }

    /**
     * Logout from session (destroy session)
     * @param {String} sessionId Session to logout from
     */
    static async logout(sessionId) {
        const session = await this.findOne({ where: { id: sessionId } });
        if (session) await session.destroy();
    }

    /**
     * Destroy all expired sessions
     */
    static async destroyExpired() {
        await this.destroy({ where: { expires: { [Op.lt]: Date.now() } } });
    }
}

const init = sequelize => {
    Session.init(
        {
            id: {
                type: DataTypes.STRING(88),
                primaryKey: true,
            },
            expires: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: Sequelize.NOW,
            },
            // userId: {
            //     type: DataTypes.INTEGER,
            //     allowNull: false,
            //     references: {
            //         model: User,
            //         key: 'id'
            //     }
            // }
        },
        {
            sequelize,
            modelName: 'session',
        }
    );

    return Session;
};

module.exports = init;
