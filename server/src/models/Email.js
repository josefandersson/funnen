const { Model, DataTypes } = require('sequelize');

/**
 * All sent, to-be-sent and received emails
 */
class Email extends Model {}

const init = sequelize => {
    Email.init(
        {
            isIncoming: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
            // format: 'name <e-mail>'
            to: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            // format: 'name <e-mail>'
            from: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            subject: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            text: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            html: DataTypes.TEXT,
            sentAt: DataTypes.DATE,
            type: DataTypes.ENUM('REGISTER')
        },
        {
            sequelize,
            modelName: 'email',
        }
    );

    return Email;
};

module.exports = init;
