const { Sequelize, Model, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');

const config = require('../../config');

class User extends Model {
    /**
     * Get altered user object that can be shared with client
     * @returns {Object} Safe user object
     */
    safe() {
        return {
            emailAddress: this.emailAddress,
            emailVerified: this.emailVerified,
            lang: this.lang,
            lostsLeft: this.lostsLeft,
            unlimitedLosts: this.unlimitedLosts,
            maxCodes: this.maxCodes,
            unlimitedCodes: this.unlimitedCodes,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
        };
    }

    /**
     * Test if password matches user password
     * @param {String} plainPassword Password to test
     * @returns {Boolean} Whether matching
     */
    testPassword(plainPassword) {
        return bcrypt.compareSync(plainPassword, this.password);
    }
}

const init = sequelize => {
    User.init(
        {
            emailAddress: {
                type: DataTypes.STRING,
                unique: true,
            },
            emailVerified: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            lang: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: 'sv',
            },
            lostsLeft: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            unlimitedLosts: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            maxCodes: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            unlimitedCodes: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
        },
        {
            sequelize,
            modelName: 'user',
            paranoid: true,
        }
    );

    // Hash password before sent to database
    User.beforeCreate('register', (model, options) => {
        model.password = bcrypt.hashSync(model.password, config.SALT_ROUNDS);
    });

    return User;
};

module.exports = init;
