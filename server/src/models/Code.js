const { Model, DataTypes } = require('sequelize');

class Code extends Model {
    /**
     * Get safe values from this object
     * @returns Safe object
     */
    safe() {
        return {
            id: this.id,
            code: this.code,
            name: this.name,
            isLost: this.isLost,
            visitCount: this.visitCount,
            lostCount: this.lostCount,
            messageCount: this.messageCount,
            createdAt: this.createdAt,
        };
    }
}

const init = sequelize => {
    Code.init(
        {
            code: {
                type: DataTypes.STRING,
                unique: true,
            },
            name: DataTypes.STRING,
            isLost: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            visitCount: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            lostCount: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            messageCount: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
        },
        {
            sequelize,
            modelName: 'code',
            paranoid: true,
        }
    );

    return Code;
};

module.exports = init;
