const { Model, DataTypes } = require('sequelize');

class LoginAttempt extends Model {}

const init = sequelize => {
    LoginAttempt.init(
        {
            ip: {
                type: DataTypes.STRING(36),
                allowNull: false,
                validate: { isIP: true },
            },
            count: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            emailAddress: {
                type: DataTypes.STRING,
                allowNull: false,
            },

            // attempt triggered too many login attempts warning
            critical: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
        },
        {
            sequelize,
            modelName: 'loginattempt',
        }
    );

    return LoginAttempt;
};

module.exports = init;
