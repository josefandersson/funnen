const { Model, DataTypes } = require('sequelize');

class EmailChange extends Model {}

const init = sequelize => {
    EmailChange.init(
        {
            code: {
                type: DataTypes.STRING,
                unique: true
            },
            newEmailAddress: {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        {
            sequelize,
            modelName: 'emailchange',
        }
    );

    return EmailChange;
};

module.exports = init;
