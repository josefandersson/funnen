const { Sequelize, Model, DataTypes } = require('sequelize');

const config = require('../config');

// Connect to MariaDB
const sequelize = new Sequelize(
    config.DEV ? config.MARIA_DB_DEV : config.MARIA_DB,
    config.MARIA_USER,
    config.MARIA_PASS,
    {
        host: config.MARIA_HOST,
        dialect: 'mariadb',
        pool: {
            max: 5,
            min: 0,
            idle: 10000,
        },
    }
);

// Import models
const Code = require('./models/Code')(sequelize);
const Email = require('./models/Email')(sequelize);
const IpAddress = require('./models/IpAddress')(sequelize);
const IpAddressBan = require('./models/IpAddressBan')(sequelize);
const LoginAttempt = require('./models/LoginAttempt')(sequelize);
const Message = require('./models/Message')(sequelize);
const User = require('./models/User')(sequelize);
const Session = require('./models/Session')(sequelize);

// Set relations
IpAddress.hasMany(User, {
    as: 'usersRegistrated',
    foreignKey: { name: 'registrationIpId', allowNull: false },
});
IpAddress.hasMany(User, { as: 'usersLast', foreignKey: { name: 'lastIpId' } });
IpAddress.hasMany(IpAddressBan);
IpAddressBan.belongsTo(IpAddress);
LoginAttempt.belongsTo(User);
User.hasMany(LoginAttempt);
Session.belongsTo(User, { allowNull: false });
User.hasMany(Session);
User.hasMany(Code);
Code.belongsTo(User, { foreignKeyConstraint: true });
Code.hasMany(Message);
Message.belongsTo(User);
User.hasMany(Message);
Email.hasMany(Email, { as: 'responseTo' });

// Update ip as banned when ip ban is created
IpAddressBan.beforeCreate('ban', async (model, options) => {
    await IpAddress.update({ banned: true }, { where: { id: model.ipId } });
});

// Sync sequelize models with database
sequelize
    .sync({
        force: config.DEV && config.FORCE_DB_SYNC,
    })
    .then(async () => {
        console.log('Synced models with database...');
    });

module.exports = {
    sequelize,

    Code,
    Email,
    IpAddress,
    IpAddressBan,
    LoginAttempt,
    User,
    Session,
};
