-- Create databases
CREATE DATABASE `funnen`;
CREATE DATABASE `funnen-dev`;

-- Create user
CREATE USER 'funnen'@'%' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON `funnen`.* TO 'funnen'@'%';
GRANT ALL PRIVILEGES ON `funnen-dev`.* TO 'funnen'@'%';
FLUSH PRIVILEGES;