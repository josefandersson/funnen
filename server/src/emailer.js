const mailer = require('nodemailer');
const jwt = require('jsonwebtoken');

const db = require('./database');
const lang = require('./lang');
const config = require('../config');

// Available email addresses
const senders = {
    ACCOUNT: {
        user: config.SMTP_ACCOUNT_USER,
        pass: config.SMTP_ACCOUNT_PASS,
    },
    CODE: {
        user: config.SMTP_CODE_USER,
        pass: config.SMTP_CODE_PASS,
    },
};

/**
 * Create nodemailer email transporter object.
 * I.e. login with smtp credentials.
 * @param {*} user
 * @param {*} pass
 */
const createTrans = (user, pass, from) =>
    new Promise((resolve, reject) => {
        const trans = mailer.createTransport(
            {
                host: config.SMTP_HOST,
                secure: false,
                auth: { user, pass },
            },
            {
                from,
            }
        );

        trans.verify(err => {
            if (err) reject(err);
            else resolve(trans);
        });
    });

// Populate 'senders' object with nodemailer transporter objects.
(async () => {
    for (let k in senders) {
        const sender = senders[k];
        sender.from = `${config.EMAIL_NAME} <${sender.user}@${config.SMTP_HOST}>`;
        sender.trans = await createTrans(sender.user, sender.pass, sender.from);
    }
})();

/**
 * Send an email to sender.
 * @param {Object} sender
 * @param {String} to
 * @param {String} text
 * @param {String|null} html
 * @param {String|null} type
 * @returns {db.Email} sent email object
 */
const send = async (sender, to, subject, text, html = null, type = null) => {
    const emailObj = await db.Email.create({
        isIncoming: false,
        to,
        from: sender.from,
        subject,
        text,
        html,
    });

    await new Promise((resolve, reject) => {
        sender.trans.sendMail(
            {
                to,
                subject,
                text,
                html,
            },
            err => {
                if (err) reject(err);
                else resolve();
            }
        );
    });

    emailObj.sentAt = new Date();
    if (type) emailObj.type = type;
    await emailObj.save();

    return emailObj;
};

/**
 * Send registration and account verification mail to user.
 * @param {db.User} user
 */
const sendRegistrationMail = async user => {
    if (user.emailVerified) return null;

    const code = jwt.sign(
        { userId: user.id },
        config.ACCOUNT_VERIFICATION_SECRET
    );
    const verLink = `https://funn.se/ver/${code}`;

    send(
        senders.ACCOUNT,
        user.emailAddress,
        lang.__({ phrase: 'register_email_subject', locale: user.lang }),
        lang.__(
            { phrase: 'register_email_text', locale: user.lang },
            { verification_link: verLink }
        ),
        lang.__(
            { phrase: 'register_email_html', locale: user.lang },
            { verification_link: verLink }
        ),
        'REGISTER'
    );
};

/**
 * Send contact mail for code to code owner.
 * @param {String} code
 * @param {String} email
 * @param {String} message
 * @param {Boolean} share
 */
const sendContactMail = async (code, email, message, share = false) => {
    const responseLink = '...';

    send(
        senders.CODE,
        code.user.emailAddress,
        lang.__({ phrase: 'email_subject_code', locale: code.user.lang }),
        lang.__(
            { phrase: 'email_text_code', locale: code.user.lang },
            { code: code.code, message, response_link: responseLink }
        ),
        lang.__(
            { phrase: 'email_html_code', locale: code.user.lang },
            { code: code.code, message, response_link: responseLink }
        )
    );
};

module.exports = {
    sendContactMail,
    sendRegistrationMail,
};
