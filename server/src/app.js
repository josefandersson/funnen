const path = require('path');
const logger = require('morgan');
const cookies = require('cookie-parser');
const bodies = require('body-parser');
const express = require('express');
const expressip = require('express-ip');
const cors = require('cors');
const config = require('../config');

require('./database');

const app = express();

app.use(logger('dev'));
app.use(expressip().getIpInfoMiddleware);
app.use(bodies.json());
app.use(cookies());

if (config.DEV)
    app.use(
        cors({
            credentials: true,
            origin: 'http://localhost:3000',
        })
    );
else app.use(express.static(path.join(__dirname, '../../client/build')));

// Validate session
app.use(require('../routes/session'));

// Login, logout, register
app.use('/auth', require('../routes/auth'));

// User space
app.use('/user', require('../routes/user'));

// Code interactions
app.use('/code', require('../routes/code'));

const indexPath = path.join(__dirname, '../../client/build', 'index.html');
app.get('(/*)?', async (req, res, next) => {
    res.sendFile(indexPath);
});

module.exports = app;
