// Start server
const { close } = require('../index');

// Run tests
require('./auth');

// Stop server
close();