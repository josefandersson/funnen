const expect = require('chai').expect;
const request = require('supertest');

const app = require('../src/app');

app.address = () => {return {}};

// Register user
describe('POST /auth/register', () => {
    it('Rejected short passwords', done => {
        request(app).post('/auth/register').send({ email:'test@josf.dev', password:'Test123', cpassword:'Test123' }).then(res => {
            const b = res.body;
            expect(b).to.contain.property('error');
            expect(b.error).to.be('password too short');
            done();
        });
    });
    it('Rejected mismatching passwords', done => {
        request(app).post('/auth/register').send({ email:'test@josf.dev', password:'Test123Test123', cpassword:'Test123' }).then(res => {
            const b = res.body;
            expect(b).to.contain.property('error');
            expect(b.error).to.be('passwords do not match');
            done();
        });
    });
    it('Could register user', done => {
        request(app).post('/auth/register').send({ email:'test@josf.dev', password:'Test123Test123', cpassword:'Test123Test123' }).then(res => {
            const b = res.body;
            expect(b).to.not.contain.property('error');
            expect(b).to.contain.property('user');
            done();
        });
    });
    it('Rejected registering existing email', done => {
        request(app).post('/auth/register').send({ email:'test@josf.dev', password:'Test123Test123', cpassword:'Test123Test123' }).then(res => {
            const b = res.body;
            expect(b).to.contain.property('error');
            expect(b.error).to.be('user already exists');
            done();
        });
    });
});
