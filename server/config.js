const fs = require('fs');

// THIS IS THE DEFAULT CONFIG
// VALUES EXPORTED FROM config.custom.js WILL OVERRIDE THESE VALUES
const defaults = {
    // HTTP
    HTTP_PORT: 5500,
    HTTP_ENABLED: true,

    // HTTPS
    HTTPS_PORT: 5501,
    HTTPS_ENABLED: false,
    HTTPS_CERT: '',
    HTTPS_KEY: '',

    // Database
    MARIA_HOST: '127.0.0.1',
    MARIA_USER: 'funnen',
    MARIA_PASS: '',
    MARIA_DB: 'funnen',
    MARIA_DB_DEV: 'funnen-dev',

    // E-mail
    EMAIL_NAME: 'Funnen',
    SMTP_HOST: '',
    SMTP_ACCOUNT_USER: 'konto',
    SMTP_ACCOUNT_PASS: null,
    SMTP_CODE_USER: 'kod',
    SMTP_CODE_PASS: null,
    ACCOUNT_VERIFICATION_SECRET: '',

    // Bcrypt
    SALT_ROUNDS: 10,

    // Authentication
    SESSION_INACTIVE_TIME: 60 * 60 * 24 * 7 * 1000,
    PASS_MIN_BYTES: 6,
    PASS_MAX_BYTES: 32,
    MAX_LOGIN_ATTEMPTS_PER_HOUR: 10,

    // Development
    DEV: process.env.NODE_ENV !== 'production',
    FORCE_DB_SYNC: false,

    // Codes
    VALID_CHARS: 'ABCDEGHJKLMNPRSTUXYZ123456789',
    CODE_LENGTH: 4,

    // Payments
    STRIPE_PUB_KEY: 'pk_',
    STRIPE_PRIV_KEY: 'sk_',
};

module.exports = {
    ...defaults,
    ...(fs.existsSync('config.custom.js') ? require('./config.custom') : {}),
};
