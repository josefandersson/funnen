const { sequelize } = require('./src/database');
const app = require('./src/app');
const config = require('./config');
const fs = require('fs');
const path = require('path');

// Start server(s)
let httpServer, httpsServer;
if (config.HTTP_ENABLED) {
    httpServer = require('http').createServer(app);
    httpServer.listen(config.HTTP_PORT, () => onListening('http', config.HTTP_PORT));
}
if (config.HTTPS_ENABLED) {
    httpsServer = require('https').createServer({
        cert: fs.readFileSync(path.resolve(__dirname, config.HTTPS_CERT)),
        key: fs.readFileSync(path.resolve(__dirname, config.HTTPS_KEY)),
    }, app);
    httpsServer.listen(config.HTTPS_PORT, () => onListening('https', config.HTTPS_PORT));
}

function onListening(proto, port) {
    console.log('Started %s server on port %d', proto, port);
}

// Print errors to console
process.on('uncaughtException', err => console.log(err));

function close() {
    httpServer?.close();
    httpsServer?.close();
    sequelize?.close();
}

module.exports = { close };