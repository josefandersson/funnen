# Setup process

1. Install maria or my db server (or have access to an external server) and node (with npm).
2. Clone repo and run `npm i` in both `./client` and `./server`.
3. Go to `./client` and run `npm run build` to build react static files.
4. Create and configure `./server/config.custom.js` with whatever values you want from `./server/config.js`.
5. Run `./server/index.js` with env var `NODE_ENV=production`.
