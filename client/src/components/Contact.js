import { useState } from 'react';

const Contact = ({ location }) => {
    const code = location.pathname.substring(1).toUpperCase();

    const [email, setEmail] = useState('');
    const [share, setShare] = useState(false);
    const [message, setMsg] = useState('');

    const onSubmit = async ev => {
        ev.preventDefault();
        const res = await fetch('/code/contact', {
            credentials: 'include',
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ code, email, share, message }),
        });
        const json = await res.json();
        if (json.error) console.warn(json.error);
    };

    return (
        <>
            <h1>Kod: {code}</h1>
            <p>
                Du har hittat en pryl som ägaren gärna vill få tillbaka! Kom i
                kontakt med ägaren genom formuläret nedan. Meddelanden delas inte med någon annan än ägaren av koden.
            </p>
            <form action='/contact' onSubmit={onSubmit}>
                <label>
                    E-mailadress 
                    <input
                        type='email'
                        value={email}
                        onChange={ev => setEmail(ev.target.value)}
                    />
                </label>
                <label>
                    Dela min e-mailadress med ägaren
                    <input
                        type='checkbox'
                        checked={share}
                        onChange={ev => setShare(ev.target.checked)}
                    />
                </label>
                <label>
                    Meddelande
                    <textarea
                        name='message'
                        cols='50'
                        rows='10'
                        value={message}
                        onChange={ev => setMsg(ev.target.value)}
                    ></textarea>
                </label>

                <p>
                    Kontrollera gärna att koden (<b>{code}</b>) stämmer innan
                    du skickar in formuläret.
                </p>
                <input type='submit' value='Skicka' />
            </form>
        </>
    );
};

export default Contact;
