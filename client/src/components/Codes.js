import { useState } from 'react';

const Codes = ({ codes, syncCodes }) => {
    /**
     * Delete a code (permanent)
     * If older than 10 seconds, asks user for confirmation first
     * @param {Object} code
     */
    const deleteCode = async code => {
        if (
            code.createdAt.getTime() < Date.now() - 10000 &&
            !window.confirm(
                `Är du säker på att du vill radera koden ${code.code}?`
            )
        )
            return;
        const res = await fetch('http://localhost:5500/code/delete', {
            credentials: 'include',
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ codeId: code.id }),
        });
        const json = await res.json();
        if (json.error) console.warn(json);
        syncCodes();
    };

    return codes.length ? (
        <table>
            <thead>
                <tr>
                    <th>Borttappad</th>
                    <th>Kod</th>
                    <th>Antal besök</th>
                    <th>Antal meddelanden</th>
                    <th>Skapad</th>
                    <th>Länk</th>
                </tr>
            </thead>
            <tbody>
                {codes.map(c => (
                    <Code key={c.id} code={c} deleteCode={deleteCode} />
                ))}
            </tbody>
        </table>
    ) : (
        <p>Du har inte skapat några koder!</p>
    );
};

const Code = ({ code, deleteCode }) => {
    const [lost, setLost] = useState(code.isLost);

    const postLost = async element => {
        const res = await fetch('/code/update', {
            credentials: 'include',
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ codeId: code.id, isLost: element.checked }),
        });
        const json = await res.json();
        if (json.error) console.warn(json.error);
        else setLost(json.isLost);
    };

    /**
     * Copy code url to clipboard
     */
    const copyUrl = () =>
        navigator.clipboard.writeText(
            `${window.location.protocol}//${window.location.host}/${code.code}`
        );

    return (
        <tr>
            <td>
                <input
                    type='checkbox'
                    checked={lost}
                    onChange={e => postLost(e.target)}
                />
            </td>
            <td>{code.code}</td>
            <td>{code.visitCount}</td>
            <td>{code.messageCount}</td>
            <td>{new Date(code.createdAt).toLocaleString()}</td>
            <td onClick={copyUrl}>{code.url}</td>
            <td>
                <button onClick={() => deleteCode(code)}>Radera</button>
            </td>
        </tr>
    );
};

export default Codes;
