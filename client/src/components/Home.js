const Home = () => {
    return (
        <>
            <h1>En förebyggande tjänst—innan dina nycklar försvinner</h1>
            <h2>Simpelt</h2>
            <ol>
                <li>Skapa ett konto</li>
                <li>Registrera en e-mailaddress som kontaktaddress</li>
                <li>
                    Fäst den genererade koden på nycklar, drönare eller annat
                    som du inte vill ha din identitet kopplad till vid
                    borttappning
                </li>
                <li>
                    Om någon hittar prylen besöker de funnen.se/&lt;din kod&gt;
                    och kontaktar dig via våran e-mailproxy
                </li>
            </ol>
            <h2>Inget fuffens</h2>
            <p>
                Vi lagrar minimalt med kunduppgifter och endast för deras
                funktionella syfte:
            </p>
            <ul>
                <li>E-mailaddress och lösenord (hashat) för inloggning</li>
                <li>Förnamn (om angivet) och e-mailaddress för kontakt</li>
                <li>
                    Ipaddress och tid vid registrering, vid senaste
                    inloggningsförsök och vid senaste besök på hemsidan för att
                    motverka kontostöld och robotar
                </li>
                <li>
                    Tid, mängd, kvitto-id och betalningssätt på alla betalningar
                    för bokföring
                </li>
                <li>
                    Tid för generering av koder, antal besök, antal och tid för
                    inkomna meddelanden för statistik för användaren och oss
                </li>
            </ul>
            <p>Vi delar ingen data med utomstående förutom:</p>
            <ul>
                <li>
                    Förnamn, som är frivilligt, med alla som besöker
                    funnen.se/&lt;din kod&gt;
                </li>
                <li>
                    Betalningsuppgifter som behandlas och sparas av externa
                    tjänster (Stripe eller Swish), där deras
                    integritetspolicyser gäller
                </li>
            </ul>
            <p>Vi använder inga externa spårnings- eller reklamtjänster</p>

            <h2>Öppen källkod</h2>
            <p>
                All kod som vi kör i bakgrunden är tillgänglig{' '}
                <a href='https://gitlab.com/josefandersson/funnen'>här</a>.
                Koden är licensierad under GNU GPL v3.0—fri modifiering,
                användning och försäljning <b>men koden måste förbli öppen</b>.
            </p>
        </>
    );
};

export default Home;
