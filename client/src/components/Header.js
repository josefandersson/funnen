import { Link } from 'react-router-dom';

import { useAuth } from './AuthProvider';

const Header = () => {
    const auth = useAuth();

    const logout = async ev => {
        ev.preventDefault();
        await auth.logout();
    };

    return (
        <div className='header'>
            <div className='page'>
                <div className='left'>
                    <Link to='/'>FUNNEN</Link>
                </div>
                <div className='mid'>
                    {!auth.loading && auth.user && (
                        <Link to='/användare'>Mitt konto</Link>
                    )}
                    <Link to='/köp'>Köp</Link>
                </div>
                <div className='right'>
                    {!auth.loading &&
                        (auth.user == null ? (
                            <div className='actions'>
                                <Link to='/logga_in' className='btn'>
                                    Logga in
                                </Link>
                                <Link to='/skapa_konto' className='btn'>
                                    Skapa konto
                                </Link>
                            </div>
                        ) : (
                            <>
                                <span>
                                    Inloggad som{' '}
                                    <Link to='/användare'>
                                        {auth.user.emailAddress}
                                    </Link>
                                </span>
                                <div className='actions'>
                                    <button onClick={logout} className='btn'>
                                        Logga ut
                                    </button>
                                </div>
                            </>
                        ))}
                </div>
            </div>
        </div>
    );
};

export default Header;
