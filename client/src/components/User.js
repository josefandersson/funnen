import { useEffect, useState } from 'react';
import { useAuth } from './AuthProvider';

import Codes from './Codes';

const User = () => {
    const auth = useAuth();

    const [codes, setCodes] = useState([]);
    const [loading, setLoading] = useState(true);

    const [email, setEmail] = useState(auth.user.email);
    const [name, setName] = useState(auth.user.name || '');

    /**
     * Change name for user
     */
    const onSubmitName = async ev => {
        ev.preventDefault();
        const res = await fetch('/user/name', {
            credentials: 'include',
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ name }),
        });
        const json = await res.json();
        if (json.error) console.warn(json);
        else auth.syncUser();
    };

    /**
     * Begin email change for user
     */
    const onSubmitEmail = async ev => {
        ev.preventDefault();
    };

    /**
     * Send a new verification email
     */
    const sendVerEmail = () => {};

    /**
     * Generate a new code
     */
    const createCode = async ev => {
        ev.preventDefault();
        const res = await fetch('/code/new', {
            credentials: 'include',
            method: 'POST',
        });
        const json = await res.json();
        if (json.error) console.warn(json);
        else syncCodes();
    };

    /**
     * Sync client codes with server (real) codes
     */
    const syncCodes = async () => {
        const res = await fetch('/code/all', {
            credentials: 'include',
        });
        const json = await res.json();
        if (json.error) console.warn(json);
        else {
            json.codes.forEach(c => {
                c.createdAt = new Date(c.createdAt);
                c.url = `${window.location.host}/${c.code}`;
            });
            setCodes(json.codes);
            setLoading(false);
        }
    };

    useEffect(() => {
        syncCodes();
    }, []);

    return (
        <>
            <h1>Användarinställningar</h1>
            <h2>Konto</h2>
            {auth.user.emailVerified || (
                <span>
                    Du har inte verifierat din e-mailadress
                    <button onClick={sendVerEmail}>
                        Skicka en ny verifieringslänk
                    </button>
                </span>
            )}
            <form action='/user/name' onSubmit={onSubmitName}>
                <label>
                    Namn (synlig för alla, rekommenderas att lämna tom)
                    <input
                        type='text'
                        name='newName'
                        value={name}
                        onChange={ev => setName(ev.target.value)}
                    />
                </label>
                <input type='submit' value='Ändra' />
            </form>
            <form action='/user/email' onSubmit={onSubmitEmail}>
                <label>
                    E-mail
                    <input
                        type='email'
                        name='newEmail'
                        value={email}
                        onChange={ev => setEmail(ev.target.value)}
                    />
                </label>
                <input type='submit' value='Ändra' />
            </form>
            <div>
                Lösenord
                <button>Ändra</button>
            </div>
            <h2>Koder</h2>
            {loading || <Codes codes={codes} syncCodes={syncCodes} />}
            <button onClick={createCode}>Skapa ny kod</button>
            <h2>Betalningar</h2>
            <p>Kvarstående antal borttappningar: 0</p>
        </>
    );
};

export default User;
