import { createContext, useContext, useState, useEffect } from 'react';

const authContext = createContext();

const AuthProvider = ({ children }) => {
    const auth = useAuthProvider();
    return <authContext.Provider value={auth}>{children}</authContext.Provider>;
};

export const useAuth = () => {
    return useContext(authContext);
};

const useAuthProvider = () => {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    /**
     * Attempt to login with credentials
     * @param {String} email
     * @param {String} password
     */
    const login = async (email, password) => {
        const res = await fetch('/auth/login', {
            credentials: 'include',
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ email, password }),
        });
        const json = await res.json();
        if (json.error) throw json.error;
        setUser(json.user);
        return json;
    };

    /**
     * Logout from current session
     */
    const logout = async () => {
        const res = await fetch('/auth/logout', {
            credentials: 'include',
            method: 'POST',
        });
        const json = await res.json();
        if (json.error) throw json.error;
        setUser(null);
        return null;
    };

    /**
     * Attempt to register an account with credentials
     * @param {String} email
     * @param {String} password
     * @param {String} cassword
     */
    const register = async (email, password, cpassword) => {
        const res = await fetch('/auth/register', {
            credentials: 'include',
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify({ email, password, cpassword }),
        });
        const json = await res.json();
        if (json.error) throw json.error;
        setUser(json.user);
        return json;
    };

    /**
     * Sync client (local) user object with server (real) user object
     */
    const syncUser = async () => {
        const res = await fetch('/user', {
            credentials: 'include',
        });
        const json = await res.json();
        if (json.error) {
            console.warn(json);
            setUser(null);
        } else setUser(json);
        setLoading(false);
    };

    useEffect(() => {
        syncUser();
    }, []);

    return {
        loading,
        login,
        logout,
        register,
        syncUser,
        user,
    };
};

export default AuthProvider;
