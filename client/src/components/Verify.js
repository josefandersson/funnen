import { useEffect, useState } from 'react';
import { useAuth } from './AuthProvider';

const Verify = ({ location }) => {
    const codeStr = location.pathname.substring(5);
    const auth = useAuth();

    const [verified, setVerified] = useState(auth.user?.emailVerified || false);

    const sendVerify = async () => {
        if (verified) return;
        const res = await fetch('/user/verify', {
            credentials: 'include',
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ code: codeStr }),
        });
        const json = await res.json();
        if (json.error) {
            if (json.error === 'already verified') setVerified(true);
            else return console.warn(json.error);
        } else setVerified(true);
        auth.syncUser();
    };

    useEffect(() => {
        sendVerify();
    }, []);

    const styling = {
        margin: '25px auto',
        padding: '10px',
        width: 'fit-content',
        border: '1px solid',
        ...(verified
            ? {
                  borderColor: '#54ab3f',
                  backgroundColor: '#014001',
              }
            : {
                  borderColor: '#8c0202',
                  backgroundColor: '#540707',
              }),
    };

    return (
        <div style={styling}>
            {verified
                ? 'Tack för att du verifierade ditt konto!'
                : 'Verifierar...'}
        </div>
    );
};

export default Verify;
