import { createContext, useContext } from 'react';
import { Link } from 'react-router-dom';
import { useAuth } from './AuthProvider';
import Stripe from './Stripe';

const pricingContext = createContext();

const Pricing = () => {
    const auth = useAuth();
    const pricing = useContext(pricingContext);

    const buy = () => {};

    return (
        <>
            <h1>Kostnader</h1>
            <p>
                Inga återkommande betalningar. Vid köp av{' '}
                <i>Alternativ enkel</i> så behöver du göra ett nytt köp när dina
                antal borttappningar tar slut.
            </p>
            <div>
                <h2>Alternativ enkel</h2>
                <p>Pris: 50 kr</p>
                <ul>
                    <li>Tillgång till tjänsten</li>
                    <li>Max 3 koder</li>
                    <li>5 stycken borttappningar (där någon kontaktar dig)</li>
                </ul>
                {!auth.loading && auth.user && (
                    <button className='btn' onClick={() => buy(0)}>
                        Köp
                    </button>
                )}
            </div>
            <div>
                <h2>Alternativ familj</h2>
                <p>Pris: 500 kr</p>
                <ul>
                    <li>Tillgång till tjänsten</li>
                    <li>Max 20 koder</li>
                    <li>
                        Oändligt många borttappningar (där någon kontaktar dig)
                    </li>
                </ul>
                {!auth.loading && auth.user && (
                    <button className='btn' onClick={() => buy(1)}>
                        Köp
                    </button>
                )}
            </div>
            <div>
                <h2>Alternativ övrigt</h2>
                <p>Pris: förhandlingsbart</p>
                <ul>
                    <li>Tillgång till tjänsten på flera konton</li>
                    <li>Oändligt många koder på flera konton</li>
                    <li>
                        Oändligt många borttappningar (där någon kontaktar er)
                        på flera konton
                    </li>
                </ul>
                {!auth.loading && auth.user && (
                    <button className='btn' onClick={() => buy(2)}>
                        Köp
                    </button>
                )}
            </div>
        </>
    );
};

export default Pricing;
