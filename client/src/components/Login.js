import { useState } from 'react';
import { Redirect } from 'react-router';
import { useAuth } from './AuthProvider';

const Login = ({ location }) => {
    const auth = useAuth();

    const [email, setEmail] = useState('');
    const [password, setPass] = useState('');

    const onSubmit = ev => {
        ev.preventDefault();
        auth.login(email, password).catch(console.warn);
    };

    if (auth.loading) return null;
    if (auth.user) return <Redirect to={location.state?.from || '/'} />;

    return (
        <>
            <h1>Logga in</h1>
            <form action='/auth/login' onSubmit={onSubmit}>
                <label>
                    E-mail
                    <input
                        type='email'
                        name='email'
                        value={email}
                        onChange={ev => setEmail(ev.target.value)}
                    />
                </label>
                <label>
                    Lösenord
                    <input
                        type='password'
                        name='password'
                        value={password}
                        onChange={ev => setPass(ev.target.value)}
                    />
                </label>
                <input type='submit' value='Logga in' />
            </form>
        </>
    );
};

export default Login;
