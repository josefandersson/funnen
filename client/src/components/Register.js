import { useState } from 'react';
import { Redirect } from 'react-router';
import { useAuth } from './AuthProvider';

const Contact = ({ location }) => {
    const auth = useAuth();

    const [email, setEmail] = useState('');
    const [password, setPass] = useState('');
    const [cpassword, setCPass] = useState('');

    const onChangePass = ev => {
        setPass(ev.target.value);
        ev.target.classList[password.length < 9 ? 'add' : 'remove'](
            'too-short'
        );
    };

    const onChangeCPass = ev => {
        setCPass(ev.target.value);
        ev.target.classList[ev.target.value !== password ? 'add' : 'remove'](
            'not-matching'
        );
    };

    const onSubmit = ev => {
        ev.preventDefault();
        auth.register(email, password, cpassword).catch(console.warn);
    };

    if (auth.loading) return null;
    if (auth.user) return <Redirect to={location.state?.from || '/'} />;

    return (
        <>
            <h1>Skapa konto</h1>
            <form action='/auth/register' onSubmit={onSubmit}>
                <label>
                    E-mail
                    <input
                        type='email'
                        name='email'
                        value={email}
                        onChange={ev => setEmail(ev.target.value)}
                    />
                </label>
                <label>
                    Lösenord
                    <input
                        type='password'
                        name='password'
                        value={password}
                        onChange={onChangePass}
                    />
                </label>
                <label>
                    Upprepa lösenord
                    <input
                        type='password'
                        name='cpassword'
                        value={cpassword}
                        onChange={onChangeCPass}
                    />
                </label>
                <input type='submit' value='Skapa konto' />
            </form>
        </>
    );
};

export default Contact;
