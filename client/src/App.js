import './App.css';
import { Route, BrowserRouter as Router, Redirect, Switch } from 'react-router-dom';

import Home from './components/Home';
import User from './components/User';
import Verify from './components/Verify';
import Contact from './components/Contact';
import Header from './components/Header';
import Login from './components/Login';
import Register from './components/Register';
import Pricing from './components/Pricing';
import AuthProvider, { useAuth } from './components/AuthProvider';

function App() {
    return (
        <AuthProvider>
            <Router>
                <Route component={Header} />

                <div className="page">
                    <Route exact path='/' component={Home} />

                    <Switch>
                        <PrivateRoute path={['/user', '/användare']}>
                            <User />
                        </PrivateRoute>

                        <Route exact path='/köp' component={Pricing} />
                        <Route exact path='/logga_in' component={Login} />
                        <Route exact path='/skapa_konto' component={Register} />

                        <Route path='/ver' component={Verify} />
                        <Route path='/:code' component={Contact} />
                    </Switch>
                </div>
            </Router>
        </AuthProvider>
    );
}

const PrivateRoute = ({ children, ...rest }) => {
    const auth = useAuth();

    if (auth.loading) return null;

    return (
        <Route
            {...rest}
            render={({ location }) =>
                auth.user ? (
                    children
                ) : (
                    <Redirect
                        to={{ pathname: '/logga_in', state: { from: location } }}
                    />
                )
            }
        ></Route>
    );
};

export default App;
